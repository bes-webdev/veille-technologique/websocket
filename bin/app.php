<?php
use Chat\Chat;
use Ratchet\App;

require dirname(__DIR__) . '/vendor/autoload.php';


$app = new App('localhost', 6543);
$app->route('/chat', new Chat);
$app->run();
