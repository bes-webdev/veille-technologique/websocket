# Websocket
## Qui est-il ?
Websocket est un protocole réseau, au même titre que http ou ftp par exemple. C'est un protocole full duplex  ce qui veut dire que la communication passe dans les deux sens. 
Pour faire simple, websocket est la technologie dont vous rêviez, pour créer une communication persistante bidirectionnelle entre le server et le client. Vous vous connectez, quand le server a quelque chose pour vous, ils vous envoie un message, quand vous avez quelque chose pour lui vous lui envoyez un message. C'est (presque) aussi simple que cela.
## Langages de programmation :
Pour untiliser cette technologie nous aurons besoin d'au moins deux langages de programmation. Un script client d'une part (JS) pour créer la connexion depuis le client. Et un script server d'autre part pour gérer la connexion.
Vu que nous connaissons déjà PHP nous allons partir avec ce langage. Ce n'est pas le premier langage qui vient en tête lorsqu'on parle de websocket (on pense plutot à nodeJS, Python ou même Java) mais certaines librairies nous facilitent réellement la vie.
## Ratchet http://socketo.me/
Ratchet est la bibliothèque que nous allons utiliser pour créer nos connexions websockets. Première chose à faire, installer quelques petits dépots qui nous permettront de faire fonctionner la librairie. Pour l'installer nous allons simplement utiliser composer avec: 
composer require « cboden/ratchet ».

Je vous invite dans les fichiers en annexe pour la démonstration de la création d'un server et de plusieurs clients.
## Conclusion 
Le protocole websocket est une technologie qui devient de plus en plus présente sur internet en même temps que les sites internets remplissent des services de plus en plus applicatifs. Avoir une connexion persistante qui permet une interaction en temps réel entre un client (votre navigateur) et un server est un luxe dont il serait bête de se priver. Les applications sont nombreuses : messagerie, système de notifications, mise à jour de l'état d'un stock, ou n'importe quoi d'autre qui nécessiterait du temps réel.  
## Pour la démo
Il suffit de lancer le script app.php via php-cli `php bin/app.php`

