<?php
namespace Chat;

use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use Ratchet\WebSocket\WsConnection;

class Chat implements MessageComponentInterface
{

    protected $clients;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
    }
    /**
     * @param ConnectionInterface $conn
     * @return mixed
     */
    function onOpen(ConnectionInterface $conn)
    {
        var_dump($conn->httpRequest->getHeader('Cookie')); // récupérer les cookies d'une connexion
        // Store the new connection to send messages to later
        $this->clients->attach($conn);
    }

    /**
     * @param ConnectionInterface $conn
     * @return mixed
     */
    function onClose(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);
    }

    /**
     * @param ConnectionInterface $conn
     * @param \Exception $e
     * @return mixed
     */
    function onError(ConnectionInterface $conn, \Exception $e)
    {
        // TODO: Implement onError() method.
    }

    /**
     * @param ConnectionInterface $from
     * @param string $msg
     * @return mixed
     */
    function onMessage(ConnectionInterface $from, $msg)
    {
//        var_dump($from->httpRequest->getHeader('Cookie')); // récupérer les cookies d'une connexion
        foreach($this->clients as $client){
            //if ($client !== $from){
            $client->send($msg);
            //}
        }
    }
}
